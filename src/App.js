import React, { Component } from "react";
import { BrowserRouter, Route } from "react-router-dom";

import "./assets/img/hero-image.jpg";
import Nav from "./components/nav/index";
import Main from "./components/main/index";
import About from "./components/about/index";
import Courses from "./components/course/index";
import Footer from "./components/footer/index";


class App extends Component {
  render () {
    return (
      <BrowserRouter>
        <div className="App">
          <Nav/>

          <Route path="/" exact component={Main} />
          <Route path="/sobre" component={About} />
          <Route path="/cursos" component={Courses} />
          
          <Footer/>
        </div>
      </BrowserRouter>
    )
  }
}

export default App;
