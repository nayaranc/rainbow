import React from "react";

const Main = () => {
    return (
        <main>
            <section className="hero">
                <div className="container row">
                    <div className="hero-form col col-50">
                        <h2> Venha conhecer! </h2>
                        <p> 
                        Nossa escola possui 12 salas de aula, secretaria, sala de direção, 
                        biblioteca e várias outras novidades!
                        <br/><br/>
                        Envie seus dados para que possamos entrar em contato.
                        </p>
                        <form>
                            <input placeholder="Primeiro nome" required/>
                            <input placeholder="Último nome" required/>
                            <input placeholder="E-mail" required/>
                            <button> Enviar </button>
                        </form>
                    </div>
                    <div className="hero-content col col-50">
                        <h1> Rainbow <span>School</span> </h1>
                        <h2> O primeiro passo para uma educação de qualidade. </h2>
                    </div>

                </div>
            </section>
        </main>
    );
};

export default Main;