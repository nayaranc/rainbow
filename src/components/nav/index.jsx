import React from "react";

const Nav = () => {
    return (
        <header>
            <nav className="navbar"> 
                <div className="container row">
                    <div className="logo col col-50">
                        <span> Rainbow </span> School
                    </div>

                    <ul className="col col-50">
                        <li> <a href="/"> Inicio </a> </li>
                        <li> <a href="/sobre"> Sobre </a> </li>
                        <li> <a href="/cursos"> Cursos </a> </li>
                        <li> <a href="https://student.forleven.com/rainbow/home" target="_blank"> Área do aluno </a> </li>
                    </ul>
                </div>
               
            </nav>
        </header>
    );
};

export default Nav;