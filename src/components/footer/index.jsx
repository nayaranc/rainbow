import react from "react";

const Footer = () => {
    return (
            <footer className="all">
                <section>
                        <div className="redes-content col">
                            <ul>
                                <li> <a href="https://www.facebook.com/" target="_blank"> Facebook </a> </li>
                                <li> <a href="https://twitter.com/" target="_blank"> Twitter </a> </li>
                                <li> <a href="https://www.instagram.com/?hl=pt-br" target="_blank"> Instagram </a> </li>
                            </ul>
                        </div>
                </section>
            </footer>
    );
};

export default Footer;